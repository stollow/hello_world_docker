FROM maven:3.6.1-jdk-13 AS build
WORKDIR /home/app
COPY pom.xml .
RUN mvn package
COPY . /home/app

FROM openjdk:13
COPY --from=build /home/app/target/. .
EXPOSE 8181
CMD java -v
CMD java -jar *-jar-with-dependencies.jar