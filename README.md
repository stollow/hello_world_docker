# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Make sure you have docker on your pc by running **docker --version**

else follow this tutorial to install docker : [install docker tutorial](https://docs.docker.com/docker-for-windows/install/)

clone this repository :

https://gitlab.com/stollow/hello_world_docker.git

then open a command line in your folder(cmd,git bash,powershell...)

Execute the command :

**docker build your_image_name**

Then use docker run command :

**docker run -p 8181:8181 -it -d your_image_name**

## Exemple

to use this exemple run localhost:8181 in your browser

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 